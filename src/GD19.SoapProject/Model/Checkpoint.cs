﻿namespace GD19.SoapProject.Model
{
    public sealed class Checkpoint
    {
        public int X1 { get; }
        public int Y1 { get; }
        public int X2 { get; }
        public int Y2 { get; }

        public Checkpoint(int x1, int y1, int x2, int y2)
        {
            X1 = x1;
            Y1 = y1;
            X2 = x2;
            Y2 = y2;
        }

        public override string ToString() => $"P1[{X1},{Y1}], P2[{X2}{Y2}]";
    }
}