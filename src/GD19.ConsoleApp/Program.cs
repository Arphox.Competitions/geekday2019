﻿using GD19.Common.Settings;
using GD19.GameLogic;
using GD19.GameLogic.NetworkHandlers;
using GD19.Networking.HttpLight;
using GD19.Networking.HttpLight.ContextHandlers;
using GD19.Networking.HttpLight.Model;
using GD19.Visualizer;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;

namespace GD19.ConsoleApp
{
    class Program
    {
        private static Logger fileLogger;
        private static Logger consoleLogger;
        private static AI ai = null;
        private static LightweightHttpServer httpServerLight;

        static void Main()
        {
            try
            {
                ReInitializeFileLoggers();

                consoleLogger = new LoggerConfiguration()
                    .WriteTo.Console(
                        outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                    .CreateLogger();

                ai = new AI(fileLogger.ForContext<AI>(), consoleLogger);

                httpServerLight = new LightweightHttpServerBuilder()
                    .EnableConcurrentRequestServing()
                    .WithLogger(fileLogger.ForContext<LightweightHttpServer>())
                    .Add<ResponseTimeMeasurerContextHandler>()
                    .Add<ExceptionHandlingContextHandler>()
                    //.Add<IpBlacklistContextHandler>(new object[] { new string[] { "192.168.1.199" } })
                    .Add<IpWhitelistContextHandler>(new object[] { new string[] { "192.168.1.20" } })
                    .Add<BusinessLogicContextHandler>((Func<LightHttpRequest, LightHttpResponse>)RespondToHttp)
                    .Build();

                httpServerLight.Start();

                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
            finally
            {
                httpServerLight.Dispose();
            }
        }

        private static void ReInitializeFileLoggers()
        {
            fileLogger?.Dispose();
            fileLogger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("GD19.Networking.HttpLight.LightweightHttpServer", LogEventLevel.Information)
                .MinimumLevel.Override("GD19.Networking.HttpLight.ContextHandlers.ResponseTimeMeasurerContextHandler", LogEventLevel.Warning)
                .MinimumLevel.Override("GD19.Networking.UdpListener", LogEventLevel.Information)
                .WriteTo.File(
                    path: LoggingSettings.GeneralLogFilePath,
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
        }

        private static LightHttpResponse RespondToHttp(LightHttpRequest request)
        {
            if (HttpYouTrackHandler.CanApply(request))
            {
                ai?.Dispose();
                consoleLogger.Information("Reinitializing!");
                LoggingSettings.OpenNewContext();
                ReInitializeFileLoggers();
                httpServerLight.ChangeLogger(fileLogger.ForContext<LightweightHttpServer>());
                ai = new AI(fileLogger.ForContext<AI>(), consoleLogger);
                AppInitializer.RunMapVisualizer(ai);
            }

            if (ai == null)
                return new EmptySuccessResponse();
            else
                return ai.GetHttpResponse(request);
        }
    }
}