﻿using GD19.Networking.HttpLight.Model;
using System.Diagnostics;

namespace GD19.GameLogic.NetworkHandlers
{
    public static class HttpYouTrackHandler
    {
        public static bool CanApply(LightHttpRequest request) => request.QueryString.Count > 0 && request.QueryString["you"] != null;
        public static void Handle(LightHttpRequest request, AI ai)
        {
            string youValueRaw = request.QueryString.Get("you");
            int playerId = int.Parse(youValueRaw);

            string trackId = request.QueryString.Get("track");
            if (trackId == null)
                Debugger.Break(); // baj van a szerverrel, nem küldött track-et!

            ai.RaceContext.PlayerId = playerId;
            ai.RaceContext.TrackId = trackId;
            ai.RaceContext.IsStarted = true;
        }
    }
}