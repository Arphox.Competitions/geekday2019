﻿using GD19.Common.Settings;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GD19.GameLogic.NetworkHandlers
{
    public sealed class HttpLevelMaskRetriever
    {
        private readonly ILogger _logger;

        public HttpLevelMaskRetriever(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<int[,]> GetLevelMaskAsync(string trackId)
        {
            string addressTemplate = $"http://{NetworkSettings.IPs.ServerIP}/geekday/DRserver.php?track={{0}}";
            string address = string.Format(addressTemplate, trackId);
            _logger.Debug("Retrieving level mask '{trackid}' from '{address}'...", trackId, address);

            string rawData = null;
            using (WebClient client = new WebClient())
            {
                rawData = await client.DownloadStringTaskAsync(address).ConfigureAwait(false);
            }

            string[] rawDataRows = rawData.Split("\n", StringSplitOptions.RemoveEmptyEntries); // by SzaboZs: all rows has the same length
            int width = rawDataRows[0].Length;
            int height = rawDataRows.Length;

            int[,] matrix = new int[width, height];

            for (int i = 0; i < rawDataRows.Length; i++)
            {
                string row = rawDataRows[i];
                int[] rowValues = row.Select(ch => (int)char.GetNumericValue(ch)).ToArray();
                for (int j = 0; j < rowValues.Length; j++)
                {
                    matrix[j, i] = rowValues[j];
                }
            }

            _logger.Debug("Level mask for track '{trackid}' downloaded, dimensions: H:{h} W:{w}",
                trackId, matrix.GetLength(0), matrix.GetLength(1));

            SaveLevelMask_InParallel(matrix, trackId);
            return matrix;
        }

        private void SaveLevelMask_InParallel(int[,] matrix, string trackId)
        {
            Task.Run(() =>
            {
                StringBuilder builder = new StringBuilder(matrix.Length);
                for (int y = 0; y < matrix.GetLength(1); y++)
                {
                    for (int x = 0; x < matrix.GetLength(0); x++)
                    {
                        builder.Append(matrix[x, y]);
                    }
                    builder.AppendLine();
                }

                string path = Path.Combine(LoggingSettings.MasksFolderPath, $"{trackId}.txt");
                Directory.CreateDirectory(Path.GetDirectoryName(path)); // if needed
                File.WriteAllText(path, builder.ToString());
                _logger.Debug("Saving mask for track '{trackid}' to '{path}'", trackId, path);
            });
        }
    }
}