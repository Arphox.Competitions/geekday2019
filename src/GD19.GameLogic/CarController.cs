﻿using System;
using System.Diagnostics;
using System.Net.Sockets;

namespace GD19.GameLogic
{
    public sealed class CarController
    {
        private DateTime disabledUntil = DateTime.Today;
        private readonly UdpClient _udpClient;
        private readonly byte[] dataBuffer = new byte[4];

        public int CurrentSpeedValue { get; set; } // [0..30]
        public int CurrentAngleValue { get; set; } // [0..35]

        public const int MY_MIN_SPEED = 2;
        public const int MY_MAX_SPEED = 28;
        public const byte GLOBAL_MAX_SPEED = 30;
        public const int MIN_ANGLE = 0;
        public const int MAX_ANGLE = 35;

        public CarController(UdpClient udpClient)
        {
            _udpClient = udpClient ?? throw new ArgumentNullException(nameof(udpClient));
        }

        public void DisableForTimespan(TimeSpan timespan) => disabledUntil = DateTime.Now.Add(timespan);

        public void TryIncreaseSpeed()
        {
            CurrentSpeedValue = (byte)Math.Min(MY_MAX_SPEED, CurrentSpeedValue + 1);
            SendCurrentState();
        }

        public void TryDecreaseSpeed()
        {
            CurrentSpeedValue = (byte)Math.Max(MY_MIN_SPEED, CurrentSpeedValue - 1);
            SendCurrentState();
        }

        public void SetMaxSpeed()
        {
            CurrentSpeedValue = MY_MAX_SPEED;
            SendCurrentState();
        }

        public void Stop()
        {
            CurrentSpeedValue = 15;
            SendCurrentState();
        }

        public void SetMinSpeed()
        {
            CurrentSpeedValue = MY_MIN_SPEED;
            SendCurrentState();
        }

        public void TurnRight(int amount = 1)
        {
            if (amount > MAX_ANGLE)
                throw new ArgumentOutOfRangeException(nameof(amount), $"Amount is too big! It is: '{amount}'");

            CurrentAngleValue += amount;
            if (CurrentAngleValue > MAX_ANGLE)
                CurrentAngleValue -= MAX_ANGLE;

            SendCurrentState();
        }

        public void TurnLeft(int amount = 1)
        {
            if (amount > MAX_ANGLE)
                throw new ArgumentOutOfRangeException(nameof(amount), $"Amount is too big! It is: '{amount}'");

            CurrentAngleValue -= amount;
            if (CurrentAngleValue < MIN_ANGLE)
                CurrentAngleValue += MAX_ANGLE;

            SendCurrentState();
        }

        public void SetAngle(byte angleAmount)
        {
            if (angleAmount > MAX_ANGLE) throw new ArgumentOutOfRangeException(nameof(angleAmount), "Angle is too high");
            if (angleAmount < MIN_ANGLE) throw new ArgumentOutOfRangeException(nameof(angleAmount), "Angle is too low");

            CurrentAngleValue = angleAmount;
            SendCurrentState();
        }

        public void SetSpeed(byte speedAmount)
        {
            if (speedAmount > MY_MAX_SPEED) throw new ArgumentOutOfRangeException(nameof(speedAmount), "Speed is too high");
            if (speedAmount < MY_MIN_SPEED) throw new ArgumentOutOfRangeException(nameof(speedAmount), "Speed is too low");

            CurrentSpeedValue = speedAmount;
            SendCurrentState();
        }

        public void TryPutMine() => Send((byte)CurrentSpeedValue, (byte)CurrentAngleValue, true, false);
        public void TryShootRocket() => Send((byte)CurrentSpeedValue, (byte)CurrentAngleValue, false, true);
        public void TryPutMineAndShootRocket() => Send((byte)CurrentSpeedValue, (byte)CurrentAngleValue, true, true);

        public void Send(byte speed, byte angle, bool tryPutMineDown = false, bool tryShootRocket = false)
        {
            TimeSpan diff = disabledUntil.Subtract(DateTime.Now);
            if (diff.TotalMilliseconds > 0)
            {
                Debug.WriteLine($"We are still disabled for {diff.TotalMilliseconds} more milliseconds");
                return;
            }

            dataBuffer[0] = speed;
            dataBuffer[1] = angle;
            dataBuffer[2] = tryPutMineDown ? (byte)1 : (byte)0;
            dataBuffer[3] = tryShootRocket ? (byte)1 : (byte)0;
            _udpClient.Send(dataBuffer, dataBuffer.Length);
        }

        public void OverrideCurrentSpeed(byte speed)
        {
            CurrentSpeedValue = speed;
        }

        public void OverrideCurrentAngle(byte angle)
        {
            CurrentAngleValue = angle;
        }

        private void SendCurrentState() => Send((byte)CurrentSpeedValue, (byte)CurrentAngleValue);
    }
}