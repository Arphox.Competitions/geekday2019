﻿using GD19.Common.Settings;
using GD19.GameLogic.Model;
using GD19.GameLogic.NetworkHandlers;
using GD19.GameLogic.UdpInput;
using GD19.Networking.HttpLight.Model;
using GD19.SoapProject;
using GD19.SoapProject.Model;
using Serilog;
using System;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace GD19.GameLogic
{
    public sealed class AI : IDisposable
    {
        private readonly ILogger _fileLogger;
        private readonly ILogger _consoleLogger;

        public RaceContext RaceContext { get; set; }
        public GameMap GameMap { get; }
        public CarController CarController { get; }

        private UdpInputProcessor udpInputProcessor;

        public AI(ILogger fileLogger, ILogger consoleLogger)
        {
            _fileLogger = fileLogger ?? throw new ArgumentNullException(nameof(fileLogger));
            _consoleLogger = consoleLogger ?? throw new ArgumentNullException(nameof(consoleLogger));

            GameMap = new GameMap(fileLogger.ForContext<GameMap>(), this);
            RaceContext = new RaceContext();
            CarController = CreateCarController();

            udpInputProcessor = new UdpInputProcessor(fileLogger.ForContext<UdpInputProcessor>(), this);
            udpInputProcessor.StartListening();
        }

        public LightHttpResponse GetHttpResponse(LightHttpRequest request)
        {
            if (!GameMap.IsInitialized)
                return InitializeIfCan(request);

            if (HttpNewBonusHandler.CanApply(request))
                GameMap.AddBonus(HttpNewBonusHandler.Handle(request));
            else if (HttpBonusGoneHandler.CanApply(request))
                HttpBonusGoneHandler.Handle(request, GameMap);

            return new EmptySuccessResponse();
        }

        private CarController CreateCarController()
        {
            var udpSender = new UdpClient();
            udpSender.Connect(NetworkSettings.IPs.ServerIP, NetworkSettings.Udp.ServerPort);
            return new CarController(udpSender);
        }

        private LightHttpResponse InitializeIfCan(LightHttpRequest request)
        {
            if (HttpYouTrackHandler.CanApply(request))
            {
                HttpYouTrackHandler.Handle(request, this);
                Task levelMaskDownloaderTask = GetLevelMaskAsync(_fileLogger);
                Task getCheckpointsTask = GetCheckpointsAsync(_fileLogger);
                Task.WaitAll(levelMaskDownloaderTask, getCheckpointsTask);
                _consoleLogger.Information("Initializing our map...");
                GameMap.InitializeOurMap();
                GameMap.IsInitialized = true;
                _consoleLogger.Information("Race initialized: Track: '{tr}'", RaceContext.TrackId);
                _fileLogger.Information("Race initialized: Track: '{tr}'", RaceContext.TrackId);
            }

            return new EmptySuccessResponse();
        }

        private async Task GetLevelMaskAsync(ILogger fileLogger)
        {
            var retriever = new HttpLevelMaskRetriever(fileLogger.ForContext<HttpLevelMaskRetriever>());
            GameMap.LevelMask = await retriever.GetLevelMaskAsync(RaceContext.TrackId).ConfigureAwait(false);
        }

        private async Task GetCheckpointsAsync(ILogger fileLogger)
        {
            Checkpoint[] checkpoints = await CheckpointRetriever.GetCheckpointsAsync(fileLogger.ForContext<CheckpointRetriever>(),
                RaceContext.TrackId).ConfigureAwait(false);

            GameMap.Checkpoints = checkpoints;
        }

        public void Dispose() => udpInputProcessor.Dispose();
    }
}