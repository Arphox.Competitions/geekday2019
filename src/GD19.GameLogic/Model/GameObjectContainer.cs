﻿using System;
using System.Collections.Generic;

namespace GD19.GameLogic.Model
{
    public sealed class GameObjectContainer
    {
        public DateTime CreatedAt { get; }
        public Dictionary<(int, int), List<GameObject>> Items { get; }

        public GameObjectContainer(Dictionary<(int, int), List<GameObject>> items)
        {
            Items = items;
            CreatedAt = DateTime.Now;
        }
    }
}