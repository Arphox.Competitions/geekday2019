﻿namespace GD19.GameLogic.Model
{
    public static class BonusType
    {
        public const char Hp = 'H';
        public const char Rocket = 'R';
        public const char Mine = 'M';
    }
}