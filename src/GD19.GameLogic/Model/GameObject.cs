﻿namespace GD19.GameLogic.Model
{
    public abstract class GameObject
    {
        public int X { get; }
        public int Y { get; }
        public int? PlayerId { get; }
        public byte Speed { get; }

        protected GameObject(int x, int y, int? playerId, byte speed)
        {
            X = x;
            Y = y;
            PlayerId = playerId;
            Speed = speed;
        }
    }
}