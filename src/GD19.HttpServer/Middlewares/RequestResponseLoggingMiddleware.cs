﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace GD19.HttpServer.Middlewares
{
    // Based on: https://exceptionnotfound.net/using-middleware-to-log-requests-and-responses-in-asp-net-core/
    public class RequestResponseLoggingMiddleware
    {
        private static readonly string NL = Environment.NewLine;
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestResponseLoggingMiddleware> _logger;

        public RequestResponseLoggingMiddleware(
            RequestDelegate next, ILogger<RequestResponseLoggingMiddleware> logger)
        {
            _next = next;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Invoke(HttpContext context)
        {
            string request = await FormatRequest(context.Request).ConfigureAwait(false);
            var originalBodyStream = context.Response.Body;

            using var responseBody = new MemoryStream();
            context.Response.Body = responseBody;
            await _next(context).ConfigureAwait(false);

            string remoteIpAddress = context.Connection.RemoteIpAddress.ToString();

            string response = await FormatResponse(context.Response).ConfigureAwait(false);

            _logger.LogDebug($"{NL}{{Req}} (From IP: '{remoteIpAddress}')" +
                             $"{NL}{{Resp}}", request, response);

            await responseBody.CopyToAsync(originalBodyStream).ConfigureAwait(false);
        }

        private async Task<string> FormatRequest(HttpRequest request)
        {
            request.EnableBuffering();
            byte[] buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length).ConfigureAwait(false);
            string bodyText = Encoding.UTF8.GetString(buffer);
            request.Body.Position = 0;

            string method = request.Method;
            string url = $"{request.Scheme}://{request.Host}{request.Path}{request.QueryString}";
            
            string bodyPart = GetBodyPart(bodyText);
            return $"[REQUEST] {method} \"{url}\"{bodyPart}";
        }

        private async Task<string> FormatResponse(HttpResponse response)
        {
            response.Body.Position = 0;
            string bodyText = await new StreamReader(response.Body).ReadToEndAsync().ConfigureAwait(false);
            response.Body.Position = 0;

            int code = response.StatusCode;
            string bodyPart = GetBodyPart(bodyText);
            return $"[RESPONSE] {code}{bodyPart}";
        }

        private static string GetBodyPart(string bodyText)
        {
            if (!string.IsNullOrEmpty(bodyText))
                return $"{NL}Body: \"{bodyText}\"";
            else
                return null;
        }
    }
}