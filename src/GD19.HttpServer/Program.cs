using GD19.GameLogic;
using GD19.HttpServer.AppInitializers;
using GD19.HttpServer.Logging;
using Serilog;
using System;
using System.Threading.Tasks;

namespace GD19.HttpServer
{
    public static class Program
    {
        public static async Task Main()
        {
            LoggerInitializer.Initialize();

            try
            {
                await RunApps().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Top-level exception occured");
                throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static Task RunApps()
        {
            Task aspTask = AspNetAppInitializer.RunAsync();
            //UdpListenerAppInitializer.StartRunningInBackground(gameLogicHub.ProcessUdpInput);
            //Visualizer.AppInitializer.Run();
            return aspTask;
        }
    }
}