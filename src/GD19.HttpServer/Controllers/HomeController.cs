﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace GD19.HttpServer.Controllers
{
    [Route("")]
    [ApiController]
    public sealed class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> logger;

        public HomeController(ILogger<HomeController> logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public string Index()
        {
            logger.LogInformation($"Hello index! :)");

            return "Hello!" + Environment.NewLine +
                  $"The time is: '{DateTime.Now}'.";
        }
    }
}