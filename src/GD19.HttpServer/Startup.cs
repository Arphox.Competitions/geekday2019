using GD19.HttpServer.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace GD19.HttpServer
{
    public sealed class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // Called by the runtime, order: 1.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        // Called by the runtime, order: 2.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSerilogRequestLogging();
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseMiddleware<RequestResponseLoggingMiddleware>();
            app.UseRouting();
            app.UseEndpoints(ep => ep.MapControllers());
        }
    }
}