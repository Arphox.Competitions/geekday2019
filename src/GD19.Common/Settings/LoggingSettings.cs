﻿using System;
using System.IO;

namespace GD19.Common.Settings
{
    public static class LoggingSettings
    {
        private const string logFilesFolderBase = @"C:/_GEEKDAY/";
        private static string currentRunFolder = DateTime.Now.ToSecondsTimestamp();

        public static string LogFilesFolder => Path.Combine(logFilesFolderBase, currentRunFolder);
        public static string GeneralLogFilePath => Path.Combine(LogFilesFolder, "log.txt");
        public static string MasksFolderPath => Path.Combine(LogFilesFolder, "masks");

        public static void OpenNewContext()
        {
            currentRunFolder = DateTime.Now.ToSecondsTimestamp();
        }
    }
}