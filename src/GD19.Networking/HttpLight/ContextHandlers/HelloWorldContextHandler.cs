﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Text;
using GD19.Networking.HttpLight.Interfaces;
using Serilog;

namespace GD19.Networking.HttpLight.ContextHandlers
{
    public sealed class HelloWorldContextHandler : IContextHandler
    {
        private readonly ILogger _logger;

        public HelloWorldContextHandler([AllowNull] ILogger logger) => _logger = logger;

        public void Handle(HttpListenerContext context)
        {
            using var ms = new MemoryStream();
            context.Request.InputStream.CopyTo(ms);
            string body = Encoding.UTF8.GetString(ms.ToArray());

            _logger?.Information("Saying hello to {ip}", context.Request.RemoteEndPoint.Address.ToString());
            byte[] responseBytes = Encoding.UTF8.GetBytes($"Hello world! at {DateTime.Now.ToString("o")}");

            context.Response.ContentLength64 = responseBytes.Length;
            context.Response.OutputStream.Write(responseBytes, 0, responseBytes.Length);
            context.Response.Close();
        }
    }
}