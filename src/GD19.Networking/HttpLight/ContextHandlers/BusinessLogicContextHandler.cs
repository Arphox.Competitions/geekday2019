﻿using System;
using System.IO;
using System.Net;
using GD19.Networking.HttpLight.Helpers;
using GD19.Networking.HttpLight.Interfaces;
using GD19.Networking.HttpLight.Model;
using Serilog;

namespace GD19.Networking.HttpLight.ContextHandlers
{
    public sealed class BusinessLogicContextHandler : IContextHandler
    {
        private readonly ILogger _logger;
        private readonly Func<LightHttpRequest, LightHttpResponse> _responderMethod;

        public BusinessLogicContextHandler(
            ILogger logger,
            Func<LightHttpRequest, LightHttpResponse> responderMethod)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _responderMethod = responderMethod ?? throw new ArgumentNullException(nameof(responderMethod));
        }

        public void Handle(HttpListenerContext context)
        {
            LightHttpRequest request = CreateRequest(context);
            RequestResponseLogger.LogRequest(_logger, request);

            LightHttpResponse response = _responderMethod(request);
            WriteResponseToContext(context, response);

            RequestResponseLogger.LogResponse(_logger, response);
        }

        private static byte[] GetBodyBytes(HttpListenerContext context)
        {
            using MemoryStream inputStream = new MemoryStream();
            context.Request.InputStream.CopyTo(inputStream);
            return inputStream.ToArray();
        }

        private LightHttpRequest CreateRequest(HttpListenerContext context)
        {
            byte[] bodyBytes = GetBodyBytes(context);
            HttpListenerRequest request = context.Request;

            return new LightHttpRequest(
                request.HttpMethod,
                request.Headers,
                request.Url,
                request.RawUrl,
                request.QueryString,
                request.RemoteEndPoint,
                bodyBytes,
                request.ContentEncoding);
        }

        private void WriteResponseToContext(HttpListenerContext context, LightHttpResponse response)
        {
            context.Response.StatusCode = response.StatusCode;
            context.Response.StatusDescription = response.StatusDescription;
            context.Response.ContentType = response.ContentType;
            if (response.Headers != null)
                context.Response.Headers = response.Headers;
            context.Response.ContentEncoding = response.ContentEncoding;
            context.Response.ContentLength64 = response.BodyBytes.Length;

            context.Response.OutputStream.Write(response.BodyBytes, 0, response.BodyBytes.Length);
            context.Response.Close();
        }
    }
}