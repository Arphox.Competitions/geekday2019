﻿using FluentAssertions;
using GD19.Networking.HttpLight;
using GD19.Networking.HttpLight.ContextHandlers;
using Serilog;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace GD19.IntegrationTests.Networking.HttpLight
{
    /// <summary>
    ///     Tests for the <see cref="LightweightHttpServer"/> class
    /// </summary>
    public sealed class LightweightHttpServerTests
    {
        private static readonly ILogger NoLogger = new LoggerConfiguration().CreateLogger();

        [Theory]
        [InlineData("http://localhost/")]
        [InlineData("http://localhost/r4nd0miz3d")]
        [InlineData("http://127.0.0.1/")]
        [InlineData("http://127.0.0.1/some/thing")]
        [ClassData(typeof(LocalIpv4AddressUrlProvider))]
        public async Task CanReachFromDifferentUrlsNotJustLocalhost(string url)
        {
            using LightweightHttpServer server = new LightweightHttpServer(new HelloWorldContextHandler(NoLogger), NoLogger);
            server.Start();
            
            using HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(new Uri(url)).ConfigureAwait(false);
            
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void ShouldNotBeListening_AfterDispose(bool startServer)
        {
            using LightweightHttpServer server = new LightweightHttpServer(new HelloWorldContextHandler(NoLogger), NoLogger);
            if (startServer)
                server.Start();

            server.Dispose();
            server.IsListening.Should().BeFalse();
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void IsDisposedShouldBeTrue_AfterDisposed(bool startServer)
        {
            using LightweightHttpServer server = new LightweightHttpServer(new HelloWorldContextHandler(NoLogger), NoLogger);
            if (startServer)
                server.Start();

            server.Dispose();
            server.IsDisposed.Should().BeTrue();
        }
    }
}