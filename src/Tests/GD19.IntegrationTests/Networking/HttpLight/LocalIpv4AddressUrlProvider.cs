﻿using GD19.Common;
using System.Collections;
using System.Collections.Generic;

namespace GD19.IntegrationTests.Networking.HttpLight
{
    public sealed class LocalIpv4AddressUrlProvider : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            string ip = IpHelper.GetLocalIPv4Address();
            string url = $"http://{ip}";
            yield return new[] { url };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}