﻿using GD19.Common.Settings;
using GD19.Networking;
using Serilog;
using System;
using System.Text;
using System.Threading.Tasks;

namespace GD19.UdpMessageListener
{
    class Program
    {
        private const int TIMEOUT_SECONDS = 3;

        static void Main()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("This is the UDP listener.");
            Console.WriteLine($"Starting listening for {TIMEOUT_SECONDS} seconds...");
            Console.ResetColor();

            ILogger dummyLogger = new LoggerConfiguration().CreateLogger();

            int port = NetworkSettings.Udp.ListeningPort;
            Action<byte[]> callback = data => Console.WriteLine(Encoding.UTF8.GetString(data));
            using UdpListener listener = new UdpListener(port, callback, dummyLogger);

            Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(TIMEOUT_SECONDS)).ConfigureAwait(false);
                SetForegroundToBlue(() => Console.WriteLine(">>> Disposing now <<<"));
                listener.Dispose();
            });

            //RunSync(listener);
            RunBackground(listener);

            Console.WriteLine("End of main! Press any key to continue...");
            Console.ReadLine();
        }

        private static void RunSync(UdpListener listener)
        {
            listener.RunSynchronously();
        }

        private static void RunBackground(UdpListener listener)
        {
            listener.RunInBackground();
        }

        private static void SetForegroundToBlue(Action action)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            action();
            Console.ResetColor();
        }
    }
}