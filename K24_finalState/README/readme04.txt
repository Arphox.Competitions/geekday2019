IMPORTANT NOTE
- currently, you are notified (via HTTP) if a new bonus appears
- however, you are NOT notified if the bonus is consumed by someone
- expect a future change in the protocol: there will be a new message via HTTP: you WILL be notified if the bonus is consumed by someone
