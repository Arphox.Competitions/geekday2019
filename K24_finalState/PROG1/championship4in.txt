﻿[Championship]
Name=Formula One World Championship
Year=2018

[Score]
1=25
2=18
3=15
4=12
5=10
6=8
7=6
8=4
9=2
10=1
*=0

[Circuits]
Circuit=Melbourne Grand Prix Circuit;Melbourne, Australia;Street;Clockwise;5303;16
Circuit=Bahrain International Circuit;Sakhir, Bahrain;Race;Clockwise;5412;23
Circuit=Shanghai International Circuit;Shanghai, China;Race;Clockwise;5451;16
Circuit=Baku City Circuit;Baku, Azerbaijan;Street;AntiClockwise;6003;20
Circuit=Circuit de Barcelona-Catalunya;Montmeló, Spain;Race;Clockwise;4655;16
Circuit=Circuit de Monaco;Monte Carlo, Monaco;Street;Clockwise;3337;19
Circuit=Circuit Gilles Villeneuve;Montreal, Canada;Street;Clockwise;4361;14
Circuit=Circuit Paul Ricard;Le Castellet, France;Race;Clockwise;5842;15
Circuit=Red Bull Ring;Spielberg bei Knittelfeld, Austria;Race;Clockwise;4318;10
Circuit=Silverstone Circuit;Silverstone, United Kingdom;Race;Clockwise;5891;18
Circuit=Hockenheimring;Hockenheim, Germany;Race;Clockwise;4574;17
Circuit=Hungaroring;Mogyoród, Hungary;Race;Clockwise;4574;14
Circuit=Circuit de Spa-Francorchamps;Stavelot, Belgium;Race;Clockwise;7004;19
Circuit=Autodromo Nazionale Monza;Monza, Italy;Race;Clockwise;5793;11
Circuit=Marina Bay Street Circuit;Singapore;Street;AntiClockwise;5063;23
Circuit=Sochi Autodrom;Sochi, Russia;Road;Clockwise;5848;18
Circuit=Suzuka International Racing Course;Suzuka, Japan;Race;AntiClockwise;5807;18
Circuit=Circuit of the Americas;Austin, United States;Race;AntiClockwise;5513;20
Circuit=Autódromo Hermanos Rodríguez;Mexico City, Mexico;Race;Clockwise;4304;17
Circuit=Autódromo José Carlos Pace;São Paulo, Brazil;Race;AntiClockwise;4309;15
Circuit=Yas Marina Circuit;Abu Dhabi, United Arab Emirates;Race;AntiClockwise;5554;21

[Drivers]
Driver=Sebastian Vettel;32;German;Ferrari;HardCore;2975;238;57
Driver=Kimi Räikkönen;40;Finnish;Ferrari;Core;1847;312;18
Driver=Sergio Pérez;29;Mexican;Mercedes;Noob;572;176;0
Driver=Esteban Ocon;23;French;Force India;Noob;136;50;0
Driver=Romain Grosjean;33;French;Haas-Ferrari;Casual;389;163;0
Driver=Kevin Magnussen;27;Danish;Haas-Ferrari;Noob;157;100;0
Driver=Stoffel Vandoorne;27;Belgian;McLaren;Noob;26;42;0
Driver=Fernando Alonso;38;Spanish;McLaren;Core;1899;314;22
Driver=Lewis Hamilton;34;United Kingdom;Mercedes;HardCore;3381;247;87
Driver=Valtteri Bottas;30;Finnish;Mercedes;Core;1250;137;10
Driver=Daniel Ricciardo;30;Australian;Red Bull Racing-Honda;Core;1024;168;3
Driver=Max Verstappen;22;Dutch;Red Bull Racing-Honda;Core;890;99;1
Driver=Nico Hülkenberg;32;German;Renault;Noob;509;176;1
Driver=Carlos Sainz Jr.;25;Spanish;Renault;Noob;247;99;0
Driver=Marcus Ericsson;29;Swedish;Sauber;Noob;18;97;0
Driver=Charles Leclerc;22;Monégasgue;Sauber;Casual;275;39;7
Driver=Pierre Gasly;23;French;Torro Rosso;Noob;106;44;0
Driver=Brendon Hartley;29;New Zealander;Torro Rosso;Noob;4;25;0
Driver=Lance Stroll;20;Canadian;Martini;Noob;67;59;0
Driver=Sergey Sirotkin;24;Russian;Martini;Noob;1;21;0


